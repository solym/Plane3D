//文件编码格式(默认为UTF8)
//文档平台格式(默认为WINDOWS格式)
//文档作者 山鹏, 时间 2014年11月29日15:13:38
//开始菜单场景的 菜单layer

#ifndef __C_StartMenuLayer_H_
#define __C_StartMenuLayer_H_

#include "cocos2d.h"

USING_NS_CC;
class C_StartMenuLayer :public Layer{
public:
    CREATE_FUNC(C_StartMenuLayer);
    virtual bool init();
    enum items{license, credit, startGame, guide, menu};

    void showMenu();//展示 所有菜单
    void licenseMenu_CallBack(Ref *); //license按钮回调方法
    void creditsMenu_CallBack(Ref *);//credits按钮回调方法
    void startGameMenu_CallBack(Ref *);//开始按钮回调方法
    void guideMenu_CallBack(Ref*);//新手向导按钮回调方法
    bool guideTouchBegan(Touch*, Event*);
    void guideTouchEnded(Touch*, Event*);
    ClippingNode *clip;//遮罩层
    Node *front;//遮罩模板容器
    Label *lbl;//向导提示文字
    Vec2 clipPoint;//要突出显示位置
};


#endif //__C_StartMenuLayer_H_
