//文件编码格式(默认为UTF8)
//文档平台格式(默认为WINDOWS格式)
//文档作者 山鹏, 时间 2014年12月1日
//主角 爆炸或通关后, 战绩 layer
//将 加入 排行榜

#ifndef __C_GameOverLayer_H_
#define __C_GameOverLayer_H_

#include "cocos2d.h"

USING_NS_CC;

class C_GameOverLayer: public Layer {
public:
    static C_GameOverLayer* create(int);//初始化参数为 战绩(int)
    virtual bool init(int);
    void backMenuCallBack(Ref*);// 返回 开始菜单 场景
    void playagainMenuCallBack(Ref*);//重置 主战斗场景
};


#endif //__C_GameOverLayer_H_
