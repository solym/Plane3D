//	Encode	USC-2 little Endian
//	format	Dos/windows
//文档作者 山鹏, 时间 2014年11月28日15:17:53
#ifndef __C_LoadingScene_H_
#define __C_LoadingScene_H_

#ifndef winSize
#define winSize Director::getInstance()->getVisibleSize()
#endif

#define winCenter Vec2(winSize.width/2, winSize.height/2)

#define TOTAL_PIC_NUM			13//要加载的图片资源个数
#define TOTAL_MUSIC_NUM			6//要加载的音乐个数
#define PRELOAD_FODDER_COUNT	18//要加载的敌机数目
#define PRELOAD_FODDERL_COUNT	3//要加载的敌机的数目
#define PRELOAD_BIGDUDE_COUBR	5//要加载的敌机的数目
#define PRELOAD_MISSILE_COUNT	5//要加载的导弹的数目
#define PRElOAD_BOSS_COUNT		1//要加载的boss的数目

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

class C_LoadingScene :public Layer{
public:
    CREATE_FUNC(C_LoadingScene);//创建主层
    virtual bool init();//初始化，用来初始化背景，加载旋转精灵，加载资源
    static Scene* createScene();//创建新场景
    void update(float dt);//更新函数，//用来加载飞机，子弹，boss
//    static int audioloaded;//声音资源是否加
//    totalNum(TOTAL_PIC_NUM){};//构造函数
private:
    void init_Background();//初始化背景
    void init_Coco();//初始化旋转精灵
    void Loading_Resource();//加载资源
    void Loading_Music();//加载声音函数
    void Loading_Pic();//加载图片资源
//    void LoadingEnemy(int type);//根据类型加载敌机
//    void LoadingBullet(int type);//根据类型加载子蛋
    void Loading_Particle();//加载粒子效果
    void Loading_Callback(Ref*);//资源加载完成后的回调函数
//    void GotoNextScene();//切换场景
//    void RunNextScene(float dt);//切换场景函数
    void setProgressbar();//初始化 进度条
    int v_currentNum;//当前加载的资源数目，这里以图片个数为准
    int v_totalNum;//加载的资源总数目，以图片和音乐个数为准
    float step;//缓存进度条 每步需前进的距离
    Sprite* v_pProgress;//会动的进度条
    Label* v_pPercent;//加载资源百分比
    Sprite3D *coco;//3D椰子头
    static float cocoRotation; //3D椰子头的旋转角度
    Sprite *progress_thumb; //进度条 精灵

//    static int m_curPreload_fodder_count;//预加载敌机数目
//    static int m_curPreload_fodderL_count;//预加载敌机数目
//    static int m_curPreload_BigDude_count;//预加载敌机数目
//    static int m_curPreload_Missile_count;//预加载导弹数目
//    static int m_curPreload_Boss_count;//预加载boss数目
//    static int updatecount;//更新次数
//    static int particleloaded;//粒子是否被加载
};

#endif //__C_LoadingScene_H_
