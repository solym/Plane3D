//	2014年11月29日19:59:38
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_BulletManager.h"

C_FoeBUlletManager * C_FoeBUlletManager::bm = nullptr;
C_PlayerBUlletManager * C_PlayerBUlletManager::bm = nullptr;

//################################################################//
//获取正在使用的子弹
const std::list<C_Bullet*> C_BulletManager::get_blt_zsy()
{
	return v_blt_zsy;	//返回正使用的子弹
}


//################################################################//
//获取敌机子弹管理器
C_FoeBUlletManager* C_FoeBUlletManager::getInstance()
{
	if (C_FoeBUlletManager::bm == nullptr) {
		C_FoeBUlletManager::bm = new C_FoeBUlletManager;
	}
	return C_FoeBUlletManager::bm;
}

//################################################################//
//获取玩家子弹管理器
C_PlayerBUlletManager* C_PlayerBUlletManager::getInstance()
{
	if (C_PlayerBUlletManager::bm == nullptr) {
		C_PlayerBUlletManager::bm = new C_PlayerBUlletManager;
	}
	return C_PlayerBUlletManager::bm;
}

//获取子弹的宏定义
#define GET_BULLET(__TYPE__,__BULLET__) \
C_Bullet* ret = nullptr; \
if (v_ ## __BULLET__ ## _wsy.empty()) { \
ret = __TYPE__::create(); \
} \
else { \
ret = v_##__BULLET__##_wsy.back(); \
v_##__BULLET__##_wsy.pop_back(); \
} \
v_##__BULLET__##_zsy.push_back(ret); \
return ret \


//从敌机子弹管理器中获取一个敌机子弹（黄弹）
C_Bullet* C_FoeBUlletManager::get_bullet()
{
	//cocos2d::log("huangdan wsy= %u\t\tzsy= %u" , v_blt_wsy.size() , v_blt_zsy.size());
	GET_BULLET(C_HuangDan,blt);
	
}

//################################################################//
//从玩家子弹管理器中获取一个子弹（蓝弹）
C_Bullet* C_PlayerBUlletManager::get_bullet()
{
	//cocos2d::log("Lan  dan wsy= %u\t\tzsy= %u" , v_blt_wsy.size() , v_blt_zsy.size());
	
	GET_BULLET(C_LanDan,blt);
}


//从玩家子弹管理器获取一个导弹
C_Bullet* C_PlayerBUlletManager::get_DaoDan()
{
	//cocos2d::log("Dao  dan wsy= %u\t\tzsy= %u" , v_dd_wsy.size() , v_dd_zsy.size());
	GET_BULLET(C_DaoDan , dd);
	/*C_GenZongDan* ret = nullptr;
	if (v_dd_wsy.empty()) {
		ret = C_GenZongDan::create();
	}
	else {
		ret = v_dd_wsy.back();
		v_dd_wsy.pop_back();
	}
	v_dd_zsy.push_back(ret);
	return ret;*/
}


//################################################################//
//子弹的回收，代理，回收敌机和玩家已发出并完成使命的子弹
void C_BulletManager::recovery(C_Bullet* bt)
{
	if (bt == nullptr) {
		return;
	}
	//如果是黄色子弹，就是敌机子弹管理器去回收
	if (dynamic_cast<C_HuangDan*>(bt) != nullptr) {
		C_FoeBUlletManager::getInstance()->recovery(bt);
		return;
	}
	else {
		//由玩家子弹管理器回收
		C_PlayerBUlletManager::getInstance()->recovery(bt);
		return;
	}
}



//敌机子弹回收，有子弹回收来调用
void C_FoeBUlletManager::recovery(C_Bullet* bt)
{
	v_blt_zsy.remove(bt);
	bt->setPosition(-100.0f , -100.0f);
	v_blt_wsy.push_back(bt);
}

//玩家子弹回收
void C_PlayerBUlletManager::recovery(C_Bullet* bt)
{
	if (dynamic_cast<C_DaoDan*>(bt) != nullptr) {
		v_dd_zsy.remove(bt);
		bt->setPosition(-100.0f , -100.0f);
		v_dd_wsy.push_back(bt);
		return;
	}
	else {
		v_blt_zsy.remove(bt);
		bt->setPosition(-100.0f , -100.0f);
		v_blt_wsy.push_back(bt);
	}
}


//################################################################//
//根据传入点和碰撞半径进行碰撞检测，并回收子弹
int C_BulletManager::crash_recovery(Point pt , float dist)
{
	if (v_blt_wsy.empty()) {
		return 0;
	}
	std::vector<C_Bullet*> dhs;	//待回收的
	Size winsize = Director::getInstance()->getWinSize();	//获取设计窗口尺寸
	Rect winrect = Rect(.0f , .0f , winsize.width , winsize.height);	//构造屏幕矩形
	//Rect ptrect = Rect(pt.x - dist , pt.y - dist , pt.x + dist , pt.y + dist);	//构造碰撞目标矩形，以矩形做碰撞即可
	
	for (auto it : v_blt_zsy) {
		Vec2 itpt = it->getPosition();
		Rect itrect = Rect(itpt.x , itpt.y, itpt.x + 0.1f , itpt.y + 0.1f);
		//如果子弹不在窗口内
		if (!winrect.intersectsRect(itrect)) {
			dhs.push_back(it);
			it->stopAllActions();	//停止所有动作，防止影响回收
			if (it->getParent()) {
				it->removeFromParentAndCleanup(false);	//摘除但不释放，可以提高效率
			}
			continue;	//下一个
		}
		//如果在目标范围内
		//if (ptrect.intersectsRect(itrect)) {
		if (pt.getDistanceSq(itpt) < dist*dist ) {
			dhs.push_back(it);
			it->stopAllActions();
			it->boom();	//显示爆炸效果
			if (it->getParent()) {
				it->removeFromParentAndCleanup(false);	//摘除但不释放，可以提高效率
			}
		}
	}// end for it:v_blt_zsy

	for each (auto it in dhs)
	{
		recovery(it);	//敌机子弹回收
	}
	return dhs.size();
}

//敌机子弹根据传入点和碰撞半径进行碰撞检测，并回收子弹
int C_FoeBUlletManager::crash_recovery(Point pt , float dist)
{
	return C_BulletManager::crash_recovery(pt,dist);
}

//玩家子弹根据传入点和碰撞半径进行碰撞检测，并回收子弹
int C_PlayerBUlletManager::crash_recovery(Point pt , float dist)
{
	return C_BulletManager::crash_recovery(pt , dist);
	//导弹特殊一点，回收需要追踪
}