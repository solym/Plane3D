//	2014年11月29日 14:07:19
//	Encode	GB2312
//	format	Dos/windows

#ifndef __C_Plane_H__
#define __C_Plane_H__

#include "C_DrawLine3D.h"
#include "cocos2d.h"
USING_NS_CC;

//################################################################################//

//飞机类     
class C_Plane : public Sprite3D
{
public:
	virtual void shoot() {};	//发射子弹接口
	virtual int get_smz() { return v_smz; }//获取生命值
	virtual void set_smz(int smz) { v_smz = smz; }	//设置生命值
	virtual void loss_smz(int loss) { v_smz -= loss; }//损失生命值
	virtual void reset_smz() { v_smz = 100; }	//重置生命值
	virtual void boom();	//爆炸接口
protected:
	int v_smz=100;	//存活值，生命值
};

//################################################################################//
//开场飞机
class C_StartPlane :public C_Plane
{
public:
	CREATE_FUNC(C_StartPlane);
	bool init();

	void automatic_rotate();
};

//################################################################################//

//敌人飞机类
class C_FoePlane :public C_Plane
{
public:
	enum TYPE :char {PaoHui,ZhengChang,Boss};
	static C_FoePlane* create(TYPE type);
	bool init();
	virtual void die();			//死亡接口，虚方法
	virtual void automatic_fly();	//自动飞行
	void shoot();	//发射子弹接口
	TYPE get_type() { return v_type; }	//获取飞机类型
protected:
	TYPE v_type;	//保存飞机类型
};

//################################################################################//

//玩家飞机类
class C_PlayerPlane :public C_Plane
{
public:
	CREATE_FUNC(C_PlayerPlane);
	bool init();
	virtual void die();	//死亡接口，虚方法
	virtual void reg_touch();	//注册触摸事件
	virtual void enter_flip();	//进场翻转效果
	virtual void shoot();		//发射蓝色子弹
	virtual void shoot(Node* , float dist = 20.f);	//发射导弹
};




#endif // !__C_Plane_H__





