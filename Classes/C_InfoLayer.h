//	2014年11月26日
//	Encode	USC-2 little Endian
//	format	Dos/windows


#ifndef __C_InfoLayer_H_
#define __C_InfoLayer_H_

#include "cocos2d.h"
#include "string.h"


#define  winSize (Director::getInstance()->getWinSize())

class C_InfoLayer :public  cocos2d::Layer
{
public:
    static C_InfoLayer* create(std::string const & filename);
    virtual bool init(std::string const & filename);
    bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
};


#endif //__C_InfoLayer_H_
