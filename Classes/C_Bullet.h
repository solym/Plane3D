//2014年1月9日14:04:45
//	Encode	USC-2 little Endian
//	format	Dos/windows

#ifndef __C_BULLET_H__
#define __C_BULLET_H__

#include "cocos2d.h"
USING_NS_CC;

//子弹基类
class C_Bullet :public Sprite
{
public:
	virtual void boom(float strength=0.3);		//爆炸效果,参数为爆炸强度，实际是烟雾的缩放
	virtual void fly_to(float angle);			//飞出去，并往哪里飞。角度为相对于屏幕上往右偏向
	virtual void fly_to(Point to);				//飞出去，并往哪里飞。终点位置为to
	virtual void reset() {};						//重置子弹参数，回收后再利用
	void set_speed(float s) { v_speed = s; }	//设置子弹速度
	float get_speed(){	return v_speed; }		//获取子弹速度
protected:
	C_Bullet() {};								//防止直接创建对象，只有继承的子类才能创建对象
	~C_Bullet() { this->release(); }			//释放
	float		v_speed;		//子弹速度
};

//改写这个宏定义，子弹不被释放，手动管理
#define CREATE_FUNC_BULLET(__TYPE__) \
static __TYPE__* create() \
{ \
__TYPE__ *pRet = new __TYPE__(); \
if (pRet && pRet->init()) \
{ \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = NULL; \
return NULL; \
} \
}


//黄色子弹
class C_HuangDan :public C_Bullet
{
public:
	CREATE_FUNC_BULLET(C_HuangDan);
	bool init();	//必须重载init，因为其在Sprite中是protected的，的create中需要使用
};

//蓝色子弹
class C_LanDan :public C_Bullet
{
public:
	CREATE_FUNC_BULLET(C_LanDan);
	bool init();	//必须重载init，因为其在Sprite中是protected的，的create中需要使用
};

//跟踪导弹类
class C_DaoDan :public C_Bullet
{
public:
	CREATE_FUNC_BULLET(C_DaoDan);
	bool init();
	void fly_to(Point to);	//飞向目标
	void fly_to(Node*,float dist=20.0f);		//跟踪飞向Node节点
protected:
	Sprite3D *	v_daodan;
	Node*		v_target;		//目标
	float		v_dist;		//目标碰撞判定距离
private:
	void zido(float);	//自动跟踪回调
};

#endif // !__C_BULLET_H__