//	2014年11月29日 14:07:19
//	Encode	USC-2 little Endian
//	format	Dos/windows

#ifndef __C_PARTICLE_H__
#define __C_PARTICLE_H__

#include "cocos2d.h"
USING_NS_CC;

class C_Particle
{
public:
	static C_Particle*getInstance();	//获取单例
	ParticleSystem *add_particle(const char*);
private:
	//私有化构造与析构，不允许继承与创建对象
	C_Particle() {};
	~C_Particle();
private:
	static C_Particle* object;	//单例对象
	std::unordered_map<std::string,ValueMap*> v_pars;
};

#endif // !__C_PARTICLE_H__


