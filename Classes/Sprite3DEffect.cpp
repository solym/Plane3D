/****************************************************************************
传智博客C++学院
****************************************************************************/

#include "Sprite3DEffect.h"
#include "3d/CCMesh.h"

static int tuple_sort(const std::tuple<ssize_t, Effect3D*, CustomCommand> &tuple1, const std::tuple<ssize_t, Effect3D*, CustomCommand> &tuple2)//tuple 元组，see botton notes
{
	return std::get<0>(tuple1) < std::get<0>(tuple2);
}

EffectSprite3D* EffectSprite3D::createFromObjFileAndTexture(const std::string &objFilePath, const std::string &textureFilePath)
{
	auto sprite = new EffectSprite3D();//新创建3d特效精灵
	if (sprite && sprite->initWithFile(objFilePath))
	{
		sprite->autorelease();
		sprite->setTexture(textureFilePath);
		return sprite;
	}
	CC_SAFE_DELETE(sprite);//如果3d特效精灵创建失败，清除其所占用内存
	return nullptr;//返回空值
}

EffectSprite3D::EffectSprite3D()
: _defaultEffect(nullptr)//默认特效置空，这种方法初始化变量可以提高效率
{

}

EffectSprite3D::~EffectSprite3D()
{
	for (auto effect : _effects)
	{
		CC_SAFE_RELEASE_NULL(std::get<1>(effect));//清理3d特效占用的内存
	}
	CC_SAFE_RELEASE(_defaultEffect);//清理3d特效占用内存
}

void EffectSprite3D::setEffect3D(Effect3D *effect)
{
	if (_defaultEffect == effect) return;//如果要设置的特效是当前默认特效，直接返回
	CC_SAFE_RETAIN(effect);//3d特效引用加1
	CC_SAFE_RELEASE(_defaultEffect);//默认3d特效释放
	_defaultEffect = effect;//默认3d特效设置为当前传进一的特效
}

void EffectSprite3D::addEffect(Effect3D* effect, ssize_t order)
{
	if (nullptr == effect) return;//如果传进来的特效为空，直接返回
	effect->retain();//特效引用加1

	_effects.push_back(std::make_tuple(order, effect, CustomCommand()));//把特效添加到特效集合里

	std::sort(std::begin(_effects), std::end(_effects), tuple_sort);//特效组元排序
}

void EffectSprite3D::eraseEffect(Effect3D* effect)
{
	auto iter = _effects.begin();//获取特效集合的第一个引用
	for (; iter != _effects.end(); ++iter)
	{
		if (std::get<1>(*iter) != effect) continue;//如果传进来的特效不等于当前特效，条过此步
		else//如果传进来的特效就是当前特效
		{
			CC_SAFE_RELEASE_NULL(std::get<1>(*iter));//特效置空
			_effects.erase(iter);//删除此特效
			break;//退出循环
		}

	}
	if (iter == _effects.end())//如果循环结束还没找到要删除的特效，打印此信息
		CCLOGWARN("Cannot find the effect in EffectSprite3D");
}

ssize_t EffectSprite3D::getEffectCount() const
{
	return _effects.size();//获取特效集合里的特效数量
}

Effect3D* EffectSprite3D::getEffect(ssize_t index) const
{
	if (index >= getEffectCount()) return nullptr;//如果要获取的特效的位置大于特效数量的总数，直接返回空值
	return std::get<1>(*(std::begin(_effects) + index));//返回第i个位置的特效
}

Effect3DOutline* Effect3DOutline::create()
{
	Effect3DOutline* effect = new Effect3DOutline();//创建3d特效边线
	if (effect && effect->init())//创建成功，返回特效
	{
		effect->autorelease();
		return effect;
	}
	else//创建失败，清理所占内存，返回空值
	{
		CC_SAFE_DELETE(effect);
		return nullptr;
	}
}

bool Effect3DOutline::init()
{

	GLProgram* glprogram = Effect3DOutline::getOrCreateProgram();//获取opengl程序
	if (nullptr == glprogram)//如果opengl程序为空
	{
		CC_SAFE_DELETE(glprogram);//清理opengl程序所占内存
		return false;//初始化失败，返回false
	}
	_glProgramState = GLProgramState::create(glprogram);//获取opengl程序的状态
	if (nullptr == _glProgramState)
	{
		return false;//如果opengl程序的状态为空值，返回false
	}
	_glProgramState->retain();//opengl程序的状态引用加1
	_glProgramState->setUniformVec3("OutLineColor", _outlineColor);//设置用户自定义的uniforms
	_glProgramState->setUniformFloat("OutlineWidth", _outlineWidth);//设置用记自定义的uniforms

	return true;
}

const std::string Effect3DOutline::_vertShaderFile = "Shaders3D/OutLine.vert";//设置顶点着色器文件
const std::string Effect3DOutline::_fragShaderFile = "Shaders3D/OutLine.frag";//设置片断着色器文件
const std::string Effect3DOutline::_keyInGLProgramCache = "Effect3DLibrary_Outline";//设置opengl缓存名字
GLProgram* Effect3DOutline::getOrCreateProgram()
{
	auto program = GLProgramCache::getInstance()->getGLProgram(_keyInGLProgramCache);//获取opengl缓存
	if (program == nullptr)//如果为空
	{
		program = GLProgram::createWithFilenames(_vertShaderFile, _fragShaderFile);//根据顶点着色器和片断着色器生成opengl程序
		GLProgramCache::getInstance()->addGLProgram(program, _keyInGLProgramCache);//把opengl缓存添加到缓存里
	}
	return program;//返回opengl程序
}

Effect3DOutline::Effect3DOutline()
: _outlineWidth(1.0f)
, _outlineColor(0, 0, 0)
{

}

Effect3DOutline::~Effect3DOutline()
{

}

void Effect3DOutline::setOutlineColor(const Vec3& color)
{
	if (_outlineColor != color)//如果边线效果跟当前使用的边线效果不一样
	{
		_outlineColor = color;///把当前使用的边线效果改为传进来的边线效果
		_glProgramState->setUniformVec3("OutLineColor", _outlineColor);//把边线效果设置进opengl里
	}
}

void Effect3DOutline::setOutlineWidth(float width)
{
	if (_outlineWidth != width)//如果传进来的边线效果宽度与当前正在使用的边线效果宽度不一样
	{
		_outlineWidth = width;//设置当前使用的边线效果宽度为传进来的宽度
		_glProgramState->setUniformFloat("OutlineWidth", _outlineWidth);//设置opengl uniforms
	}
}

void Effect3DOutline::drawWithSprite(EffectSprite3D* sprite, const Mat4 &transform)
{
	auto mesh = sprite->getMesh();//获取 3d精灵的网格模型
	int offset = 0;//偏移量设置为0
	for (auto i = 0; i < mesh->getMeshVertexAttribCount(); i++)//循环3d网格模型的顶点
	{
		auto meshvertexattrib = mesh->getMeshVertexAttribute(i);//获取第i个位置的网格模型的顶点变量

		_glProgramState->setVertexAttribPointer(s_attributeNames[meshvertexattrib.vertexAttrib], meshvertexattrib.size, meshvertexattrib.type, GL_FALSE, mesh->getVertexSizeInBytes(), (void*)offset);//把获取的顶点变量设置进opengl程序的状态里
		offset += meshvertexattrib.attribSizeBytes;//更新偏移量
	}
	//draw
	{
		glEnable(GL_CULL_FACE);//开启剔除效果操作,seet bottom notes
		glCullFace(GL_FRONT);//剔除多边形前面
		glEnable(GL_DEPTH_TEST);//开启深度测试，什么是深度测试sett bottom notes
		GL::blendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//颜色混合，see notes
		Color4F color(sprite->getDisplayedColor());//获取精灵要显示的颜色
		color.a = sprite->getDisplayedOpacity() / 255.0f;//设置精灵的显示透明度

		_glProgramState->setUniformVec4("u_color", Vec4(color.r, color.g, color.b, color.a));//把颜色设置进opengl程序的状态里

		auto mesh = sprite->getMesh();//获取精灵的网格模型
		glBindBuffer(GL_ARRAY_BUFFER, mesh->getVertexBuffer());//把精灵顶点缓存绑定到GL_ARRAY_BUFFER上
		_glProgramState->apply(transform);//设置改变
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->getIndexBuffer());//绑定精灵网格顶点缓存到opengl元素组组缓存中
		glDrawElements((GLenum)mesh->getPrimitiveType(), mesh->getIndexCount(), (GLenum)mesh->getIndexFormat(), (GLvoid*)0);//通过opegnl 来渲染元素，glDrawElements用法,see notes
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);//缓存置空
		glBindBuffer(GL_ARRAY_BUFFER, 0);//缓存置空
		glDisable(GL_DEPTH_TEST);//关闭深度测试
		glCullFace(GL_BACK);//剔除多边线后面
		glDisable(GL_CULL_FACE);//关闭剔除效果操作
	}
}

void EffectSprite3D::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	for (auto &effect : _effects)//从特效集合中取一个特效
	{
		if (std::get<0>(effect) >= 0)//如果特效元组序列索引大于等于0
			break;//直接退出循环
		CustomCommand &cc = std::get<2>(effect);//创建渲染命令
		cc.func = CC_CALLBACK_0(Effect3D::drawWithSprite, std::get<1>(effect), this, transform);//设置渲染命令的回调函数
		renderer->addCommand(&cc);//把渲染命令添加到渲染队列里

	}

	if (!_defaultEffect)//如果特效不为空
	{
		Sprite3D::draw(renderer, transform, flags);//渲染
	}
	else//如果特效为空
	{
		_command.init(_globalZOrder);//初始化渲染命令的globalZorder，这个值用来控制同一维度的渲染后显示前后顺序
		_command.func = CC_CALLBACK_0(Effect3D::drawWithSprite, _defaultEffect, this, transform);//设置渲染命令的回调函数
		renderer->addCommand(&_command);//把渲染命令添加到渲染队列里
	}

	for (auto &effect : _effects)
	{
		if (std::get<0>(effect) <= 0)//如果特效元组序列索引小于等于0
			continue;//跳过本次循环
		CustomCommand &cc = std::get<2>(effect);//获取渲染命令
		cc.func = CC_CALLBACK_0(Effect3D::drawWithSprite, std::get<1>(effect), this, transform);//设置渲染命令的回调
		renderer->addCommand(&cc);//添加渲染命令到渲染队列

	}
}
/*****************

glCullFace：指定剔出操作的多边形面

C语言描述
void glCullFace(GLenum mode);
参数
mode  指定应剔除多边形的哪一个面，不是GL_FRONT就是GL_BACK。
说明
本函数可以禁用多边形正面或背面上的光照、阴影和颜色计算及操作，消除不必要的渲染计算是因为无论对象如何进行旋转或变换，都不会看到多边形的背面。用GL_CULL_FACE参数调用glEnable和glDisable可以启用或禁用剔除。
Name
glCullFace — 指明多边形的前面或后面是否被剔除。



glEnalbe(GL_CULL_FACE) 开启剔除操作效果
glDisable(GL_CULL_FACE) 关闭剔除操作效果

剔除操作
1.glCullFace()参数包括GL_FRONT和GL_BACK。表示禁用多边形正面或者背面上的光照、阴影和颜色计算及操作，消除不必要的渲染计算。
例如某对象无论如何位置变化，我们都只能看到构成其组成的多边形的某一面时，可使用该函数。

2.glPolygonMode
　　简介
  　　glPolygonMode函数用于控制多边形的显示方式。
	　　原型是:void glPolygonMode(GLenum face,GLenum mode);
	  参数
	  　　face这个参数确定显示模式将适用于物体的哪些部分，控制多边形的正面和背面的绘图模式：
		　　GL_FRONT表示显示模式将适用于物体的前向面（也就是物体能看到的面）
		  　　GL_BACK表示显示模式将适用于物体的后向面（也就是物体上不能看到的面）
			　　GL_FRONT_AND_BACK表示显示模式将适用于物体的所有面
			  　　mode这个参数确定选中的物体的面以何种方式显示（显示模式）：
				　　GL_POINT表示只显示顶点，多边形用点显示
				  　　GL_LINE表示显示线段，多边形用轮廓显示
					　　GL_FILL表示显示面，多边形采用填充形式
					  　　例：
						　　glPolygonMode(GL_FRONT, GL_LINE);表示物体的前向面用线段显示
						  　　需要特别注意的是OpenGL3.1只接受GL_FRONT_AND_BACK作为face的值，并且不管是多边形的正面还是背面都以相同的方式渲染。
							***********************************************************************************************************
							深度测试
							（1）什么是深度？
							深度其实就是该象素点在3d世界中距离摄象机的距离（绘制坐标），深度缓存中存储着每个象素点（绘制在屏幕上的）的深度值！
							深度值（Z值）越大，则离摄像机越远。
							深度值是存贮在深度缓存里面的，我们用深度缓存的位数来衡量深度缓存的精度。深度缓存位数越高，则精确度越高，目前的显卡一般都可支持16位的Z Buffer，一些高级的显卡已经可以支持32位的Z Buffer，但一般用24位Z Buffer就已经足够了。
							（2）为什么需要深度？
							在不使用深度测试的时候，如果我们先绘制一个距离较近的物体，再绘制距离较远的物体，则距离远的物体因为后绘制，会把距离近的物体覆盖掉，这样的效果并不是我们所希望的。而有了深度缓冲以后，绘制物体的顺序就不那么重要了，都能按照远近（Z值）正常显示，这很关键。
							实际上，只要存在深度缓冲区，无论是否启用深度测试，OpenGL在像素被绘制时都会尝试将深度数据写入到缓冲区内，除非调用了glDepthMask(GL_FALSE)来禁止写入。这些深度数据除了用于常规的测试外，还可以有一些有趣的用途，比如绘制阴影等等。
							（2）启用深度测试
							使用 glEnable(GL_DEPTH_TEST);
							在默认情况是将需要绘制的新像素的z值与深度缓冲区中对应位置的z值进行比较，如果比深度缓存中的值小，那么用新像素的颜色值更新帧缓存中对应像素的颜色值。
							但是可以使用glDepthFunc(func)来对这种默认测试方式进行修改。
							其中参数func的值可以为GL_NEVER（没有处理）、GL_ALWAYS（处理所有）、GL_LESS（小于）、GL_LEQUAL（小于等于）、GL_EQUAL（等于）、GL_GEQUAL（大于等于）、GL_GREATER（大于）或GL_NOTEQUAL（不等于），其中默认值是GL_LESS。
							一般来将，使用glDepthFunc(GL_LEQUAL);来表达一般物体之间的遮挡关系。
							（3）启用了深度测试，那么这就不适用于同时绘制不透明物体。

							备注：
							绘制半透明物体时，需注意：在绘制半透明物体时前，还需要利用glDepthMask(GL_FALSE)将深度缓冲区设置为只读形式，否则可能出现画面错误。为什么呢，因为画透明物体时，将使用混色，这时就不能继续使用深度模式，而是利用混色函数来进行混合。这一来，就可以使用混合函数绘制半透明物体了。


							**********************************************************************************************************

							glBlendFunc();--混合

							OpenGL 会把源颜色和目标颜色各自取出，并乘以一个系数（源颜色乘以的系数称为“源因子”，目标颜色乘以的系数称为“目标因子”），然后相加，这样就得到了新的颜 色。（也可以不是相加，新版本的OpenGL可以设置运算方式，包括加、减、取两者中较大的、取两者中较小的、逻辑运算等，但我们这里为了简单起见，不讨 论这个了） 下面用数学公式来表达一下这个运算方式。假设源颜色的四个分量（指红色，绿色，蓝色，alpha值）是(Rs, Gs, Bs, As)，目标颜色的四个分量是(Rd, Gd, Bd, Ad)，又设源因子为(Sr, Sg, Sb, Sa)，目标因子为(Dr, Dg, Db, Da)。则混合产生的新颜色可以表示为： (Rs*Sr+Rd*Dr, Gs*Sg+Gd*Dg, Bs*Sb+Bd*Db, As*Sa+Ad*Da) 当然了，如果颜色的某一分量超过了1.0，则它会被自动截取为1.0，不需要考虑越界的问题。

							源因子和目标因子是可以通过glBlendFunc函数来进行设置的。glBlendFunc有两个参数，前者表示源因子，后者表示目标因子。这两个参数可以是多种值，下面介绍比较常用的几种。

							GL_ZERO： 表示使用0.0作为因子，实际上相当于不使用这种颜色参与混合运算。

							GL_ONE： 表示使用1.0作为因子，实际上相当于完全的使用了这种颜色参与混合运算。

							GL_SRC_ALPHA：表示使用源颜色的alpha值来作为因子。

							GL_DST_ALPHA：表示使用目标颜色的alpha值来作为因子。

							GL_ONE_MINUS_SRC_ALPHA：表示用1.0减去源颜色的alpha值来作为因子。

							GL_ONE_MINUS_DST_ALPHA：表示用1.0减去目标颜色的alpha值来作为因子。 除此以外，还有GL_SRC_COLOR（把源颜色的四个分量分别作为因子的四个分量）、GL_ONE_MINUS_SRC_COLOR、 GL_DST_COLOR、GL_ONE_MINUS_DST_COLOR等，前两个在OpenGL旧版本中只能用于设置目标因子，后两个在OpenGL 旧版本中只能用于设置源因子。新版本的OpenGL则没有这个限制，并且支持新的GL_CONST_COLOR（设定一种常数颜色，将其四个分量分别作为 因子的四个分量）、GL_ONE_MINUS_CONST_COLOR、GL_CONST_ALPHA、 GL_ONE_MINUS_CONST_ALPHA。另外还有GL_SRC_ALPHA_SATURATE。新版本的OpenGL还允许颜色的alpha 值和RGB值采用不同的混合因子。



							举例来说： 如果设置了glBlendFunc(GL_ONE, GL_ZERO);，则表示完全使用源颜色，完全不使用目标颜色，因此画面效果和不使用混合的时候一致（当然效率可能会低一点点）。如果没有设置源因子和目标因子，则默认情况就是这样的设置。 如果设置了glBlendFunc(GL_ZERO, GL_ONE);，则表示完全不使用源颜色，因此无论你想画什么，最后都不会被画上去了。（但这并不是说这样设置就没有用，有些时候可能有特殊用途） 如 果设置了glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);，则表示源颜色乘以自身的alpha 值，目标颜色乘以1.0减去源颜色的alpha值，这样一来，源颜色的alpha值越大，则产生的新颜色中源颜色所占比例就越大，而目标颜色所占比例则减 小。这种情况下，我们可以简单的将源颜色的alpha值理解为“不透明度”。这也是混合时最常用的方式。 如果设置了glBlendFunc(GL_ONE, GL_ONE);，则表示完全使用源颜色和目标颜色，最终的颜色实际上就是两种颜色的简单相加。例如红色(1, 0, 0)和绿色(0, 1, 0)相加得到(1, 1, 0)，结果为黄色。



							注意： 所 谓源颜色和目标颜色，是跟绘制的顺序有关的。假如先绘制了一个红色的物体，再在其上绘制绿色的物体。则绿色是源颜色，红色是目标颜色。如果顺序反过来，则 红色就是源颜色，绿色才是目标颜色。在绘制时，应该注意顺序，使得绘制的源颜色与设置的源因子对应，目标颜色与设置的目标因子对应。不要被混乱的顺序搞晕 。
							**************************************************************************************************************


							关于glDrawElements的使用
							glDrawElements是一个OPENGL的图元绘制函数，从数组中获得数据渲染图元。
							函数原型为：
							void glDrawElements( GLenum mode, GLsizei count,
							GLenum type, const GLvoid *indices）；
							其中：
							mode指定绘制图元的类型，它应该是下列值之一，GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_QUAD_STRIP, GL_QUADS, and GL_POLYGON.
							count为绘制图元的数量乘上一个图元的顶点数。
							type为索引值的类型，只能是下列值之一：GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, or GL_UNSIGNED_INT。
							indices：指向索引存贮位置的指针。
							glDrawElements函数能够通过较少的函数调用绘制多个几何图元，而不是通过OPENGL函数调用来传递每一个顶点，法线，颜色信息。你可以事先准备一系列分离的顶点、法线、颜色数组，并且调用一次glDrawElements把这些数组定义成一个图元序列。当调用glDrawElements函数的时候，它将通过索引使用count个成序列的元素来创建一系列的几何图元。mode指定待创建的图元类型和数组元素如何用来创建这些图元。但是如果GL_VERTEX_ARRAY 没有被激活的话，不能生成任何图元。被glDrawElements修改的顶点属性在glDrawElements调用返回后的值具有不确定性，例如，GL_COLOR_ARRAY被激活后，当glDrawElements执行完成时，当前的颜色值是没有指定的。没有被修改的属性值保持不变。
							可以在显示列表中包含glDrawElements，当glDrawElements被包含进显示列表时，相应的顶点、法线、颜色数组数据也得进入显示列表的，因为那些数组指针是ClientSideState的变量，在显示列表创建的时候而不是在显示列表执行的时候，这些数组指针的值影响显示列表。glDrawElements只能用在OPENGL1.1或者更高的版本。

							**************************************************************************************************************
							元组是一种长度固定的允许有不同类型元素的集合，根据元素的个数不同又分别称作一元组、二元组、三元组等。C++11中标准库增加了一个叫std::tuple的类模板，用于表示元组。下面的代码演示了使用C++创建一个三元组。

							c++ 11
							auto tuple = std::make_tuple(1, 'A', "传智博客");
							std::cout << std::get<0>(tuple) << std::endl;
							std::cout << std::get<1>(tuple) << std::endl;
							std::cout << std::get<2>(tuple) << std::endl;
							// 下面是C++14的特性
							std::cout << std::get<int>(tuple) << std::endl;
							std::cout << std::get<char>(tuple) << std::endl;
							std::cout << std::get<const char*>(tuple) << std::endl;
							输出

							1
							A
							传智博客
							1
							A
							传智博客


							*/
