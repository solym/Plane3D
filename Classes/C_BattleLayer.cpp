//	Encode	USC-2 little Endian
//	format	Dos/windows
//	2014年11月29日 16:24:22
//	willows

#include "C_BattleLayer.h"
#include "C_FoeManager.h"
#include "C_BulletManager.h"
#include "C_GameOverLayer.h"

//战斗层的初始化
bool C_BattleLayer::init()
{
	if (!Layer::init()) {
		return false;
	}
	//玩家飞机
	v_player = C_PlayerPlane::create();
	v_player_life = 3;	//3条命
	//滚动背景
	auto bg = C_SlideGround::create();
	addChild(bg);
	bg->slide_bg();

	//成绩和生命UI层
	v_score_ui = C_UILayer::create();

	/**
	//添加敌机
	auto dj1 = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::Boss);
	auto dj2 = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::PaoHui);
	auto dj4 = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::PaoHui);
	auto dj3 = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::ZhengChang);
	dj1->setPosition(Vec2(100 , 100));
	dj2->setPosition(Vec2(200 , 400));
	dj3->setPosition(Vec2(100 , 600));
	dj4->setPosition(Vec2(400 , 400));

	//敌机自动飞行
	dj1->automatic_fly();
	dj2->automatic_fly();
	dj3->automatic_fly();
	dj4->automatic_fly();

	
	addChild(dj1);
	addChild(dj2);
	addChild(dj3);
	addChild(dj4);
	*/

	//放在滚动背景之后添加，避免被其遮住。可以设置ZOrder
	addChild(v_player);
	v_player->setPosition(Director::getInstance()->getWinSize() / 2);
	v_player->enter_flip();

	//添加成绩显示UI
	addChild(v_score_ui);

	v_score_ui->setHP(v_player->get_smz());	//设置血量
	v_score = 0;
	v_score_ui->setScore(v_score);	//设置分数
	v_score_ui->setLocalZOrder(1234);
	

	//开启定时器，做碰撞检测与敌机回收生成
	scheduleUpdate();

	return true;
}

void C_BattleLayer::update(float dt)
{
	static int total = 0;
	total += 1;
	if (total > 30) {
		total = 0;
		v_player->shoot();
	}
	static std::vector<C_FoePlane*> dhs;	//保存上一次待回收的敌机
	//回收炮灰敌机
	for each (auto it in dhs)
	{
		it->reset_smz();	//重置生命值
		C_FoeManager::getInstance()->recovery(it);
	}
	dhs.clear();	//清空待回收序列
	
	//玩家飞机回收
	if (v_player->get_smz()< 0) {
		v_player->boom();	//爆炸
		v_player->removeFromParentAndCleanup(true);
		v_player_life -= 1;	//更新玩家生命数
		//如果还有命就继续，否则结束了
		if (v_player_life < 0) {
			//创建游戏结束层
			auto js = C_GameOverLayer::create(v_score);
			getParent()->addChild(js);	//添加到父节点上
			js->setScale(0.8f);
			unscheduleUpdate();	//停止帧循环定时器
			C_FoeManager::getInstance()->recovery_all();	//回收所有的敌机
			//removeFromParentAndCleanup(true);
			return;	//直接结束了
		}
		v_player = C_PlayerPlane::create();
		addChild(v_player);
		v_player->setPosition(Director::getInstance()->getWinSize() *0.3f);
		v_player->enter_flip();
	}//end if v_player->get_smz()

	const std::list<C_FoePlane*> dj = C_FoeManager::getInstance()->get_zysFoe();	//获取正在使用的敌机
	int score = 0;
	
	for each (auto it in dj)
	{	
		if (it->get_smz() < 0) {
			//生命值小于0，被回收
			dhs.push_back(it);
			it->stopAllActions();
			it->boom();	//敌机爆炸
			continue;
		}//end if it->get_smz...

		//回收碰撞了的子弹
		score = C_PlayerBUlletManager::getInstance()->crash_recovery(it->getPosition() , 20.0f);
		if (score > 0) {
			//被两个子弹打中的时候，要减少其血量（生命值）
			int qd = it->get_type() == C_FoePlane::TYPE::Boss ? 5.0f : 10.0f;
			it->loss_smz(qd*score);	//损失生命值
		}//end if score>0

		v_score += score*10;	//更新成绩
		
		//与玩家飞机做碰撞检测，碰到了就要双方都损失生命值
		//if (it->getBoundingBox().intersectsRect(v_player->getBoundingBox())) {
		if (it->getPosition().distanceSquared(v_player->getPosition())<400.0f) {
			//dhs.push_back(it);
			//it->boom();	//敌机爆炸
			//敌机生命值减少
			it->loss_smz(5);
			//it->stopAllActions();
			int new_smz = v_player->get_smz() - 5;
			v_player->set_smz(new_smz);	//玩家飞机生命值减少
			v_score_ui->setHP(new_smz);
			v_score += 9;	//更新成绩
		}//end if it->...
		
	}// end for each ...

	//总的敌机数目少于5个才产生
	if (C_FoeManager::getInstance()->get_zsy_count() <5/* && v_Total_ph > 0*/) {
		int paohui = rand() % 4;	//每次最多产生3个
		v_Total_ph -= paohui;	//
		for (int i = 0; i < paohui; ++i) {
			//获取一个炮灰敌机
			C_FoePlane* phdj = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::PaoHui);
			addChild(phdj);	//添加到战斗层
			phdj->setPosition(Vec2(480.0f*(i%2) ,-100.0f));
			phdj->automatic_fly();	//自动飞行
		}
		C_FoePlane* zcdj = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::ZhengChang);
		addChild(zcdj);	//添加到战斗层
		zcdj->setPosition(Vec2(480.0f*(paohui % 2) , -100.0f));
		zcdj->automatic_fly();	//自动飞行
	}
	//如果总的炮灰数低于2个，就产生大Boss
	if (v_Total_ph < 2) {
		v_Total_ph = 20;	//再来20个小弟
		C_FoePlane* boss = C_FoeManager::getInstance()->get_Foe(C_FoePlane::TYPE::Boss);
		addChild(boss);	//添加到战斗层
		boss->setPosition(Vec2(480.0f, -100.0f));
		boss->automatic_fly();	//自动飞行
	}


	//更新分数显示
	v_score_ui->setScore(v_score);
}