//	2014年11月29日 19:59:45
//	Encode	USC-2 little Endian
//	format	Dos/windows


#ifndef __C_FOEMANAGER_H_
#define __C_FOEMANAGER_H_

#include "vector"
#include "list"
#include "C_3DPlane.h"

//敌机管理类
class C_FoeManager
{
public:
	static C_FoeManager* getInstance();
	virtual C_FoePlane* get_Foe(C_FoePlane::TYPE);		//获取一个未使用的敌机
	C_FoePlane* get_one_run_foe();	//获取一个运行的敌机
	const std::list<C_FoePlane*>& get_zysFoe();	//获取正在使用的敌机
	virtual const int get_zsy_count();	//获取正使用的敌机数目
	virtual void recovery_all();	//回收所有的正使用的敌机
	void recovery(C_FoePlane*);	//回收已经使用的的可回收子弹
protected:
	C_FoeManager() {};
	~C_FoeManager() {};
protected:
	std::vector<C_FoePlane*> v_ph_wsy;	//未使用的敌机 炮灰
	std::vector<C_FoePlane*> v_zc_wsy;	//未使用的敌机 正常
	std::vector<C_FoePlane*> v_bs_wsy;	//未使用的敌机 大boss
	std::list<C_FoePlane*> v_zsy;	//正使用的敌机
private:
	static C_FoeManager *fm;
};

#endif //__C_FOEMANAGER_H_
