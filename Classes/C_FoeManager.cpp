//	2014年11月29日19:59:38
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_FoeManager.h"

//敌机管理类单例的指针
C_FoeManager* C_FoeManager::fm = nullptr;

//获取敌机管理器
C_FoeManager* C_FoeManager::getInstance()
{
	if (C_FoeManager::fm == nullptr) {
		C_FoeManager::fm = new C_FoeManager;
	}
	return C_FoeManager::fm;
}

//获取正在使用的敌机
const std::list<C_FoePlane*>& C_FoeManager::get_zysFoe()
{
	return v_zsy;
}

const int C_FoeManager::get_zsy_count()
{
	return v_zsy.size();
}

//从敌机管理器中随机获取一个正在运行的敌机（测试用的）
C_FoePlane* C_FoeManager::get_one_run_foe()
{
	if (v_zsy.empty()) {
		return nullptr;
	}
	int at = rand() % v_zsy.size();
	auto it = v_zsy.begin();
	for (int i = 0; i < at; ++i , it++);
	return *it;
}


void C_FoeManager::recovery_all()
{
	std::list<C_FoePlane*> hs = v_zsy;
	for each (auto it in hs)
	{
		recovery(it);	//回收敌机
	}
}

//回收敌机
void C_FoeManager::recovery(C_FoePlane* dj)
{
	if (dj == nullptr) {
		return;
	}
	std::vector<C_FoePlane*>* v_wsyPtr = nullptr;
	//确定回收敌机到哪一个vector中去
	switch (dj->get_type()) {
	case C_FoePlane::PaoHui:
		v_wsyPtr = &v_ph_wsy;
		break;
	case C_FoePlane::ZhengChang:
		v_wsyPtr = &v_zc_wsy;
		break;
	case C_FoePlane::Boss:
		v_wsyPtr = &v_bs_wsy;
		break;
	default:
		return;	//都不是
		break;
	}
	dj->stopAllActions();	//停止所有的动作
	v_zsy.remove(dj);	//从正使用的中间移除敌机
	v_wsyPtr->push_back(dj);	//放入未使用的vector中
	dj->setPosition(-100.0f , -100.0f);	//设置位置为屏幕外边
	if (dj->getParent()) {	//父节点不为空，判断是因为有可能获取了敌机但没有把其addChild到某个节点
		dj->removeFromParentAndCleanup(false);	//从其父节点移除，但是不做清除
	}
	return;
}


//获取一个敌机，传入要获取的类型
C_FoePlane* C_FoeManager::get_Foe(C_FoePlane::TYPE type)
{
	C_FoePlane* ret = nullptr;
	std::vector<C_FoePlane*>* v_wsyPtr = nullptr;
	switch (type) {
	case C_FoePlane::PaoHui:
		v_wsyPtr=&v_ph_wsy;
		break;	//不可少
	case C_FoePlane::ZhengChang:
		v_wsyPtr = &v_zc_wsy;
		break;
	case C_FoePlane::Boss:
		v_wsyPtr = &v_bs_wsy;
		break;
	default:
		return nullptr;
		break;
	}
	if (v_wsyPtr->empty()) {
		ret = C_FoePlane::create(type);
	}
	else {
		ret = v_wsyPtr->back();
		v_wsyPtr->pop_back();
	}
	v_zsy.push_back(ret);
	return ret;
}



