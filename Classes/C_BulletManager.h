//	2014年11月29日 19:59:45
//	Encode	USC-2 little Endian
//	format	Dos/windows


#ifndef __C_BULLETMANAGER_H_
#define __C_BULLETMANAGER_H_
#include "vector"
#include "list"
#include "C_Bullet.h"

//子弹管理基类
class C_BulletManager
{
public:
	//static C_BulletManager* getInstance();
	virtual C_Bullet* get_bullet()=0;		//获取一个未使用的子弹
	virtual const std::list<C_Bullet*> get_blt_zsy();	//获取正在使用的子弹
	virtual int crash_recovery(Point,float) = 0;	//碰撞检测并回收


	static void recovery(C_Bullet*);	//回收已经使用的的可回收子弹，代理模式
protected:
	C_BulletManager() {};	//私有化构造，避免被直接new
	virtual ~C_BulletManager() {};	//私有化析构，防止拷贝构造等操作（临时对象无法析构）
protected:
	std::vector<C_Bullet*> v_blt_wsy;	//未使用的子弹
	std::list<C_Bullet*> v_blt_zsy;	//正使用的子弹
};

/**

#define GETINSTANCE_FUNC(__TYPE__) \
static __TYPE__* getInstance() \
{ \
if (__TYPE__::bm == nullptr) { \
__TYPE__::bm = new __TYPE__; \
} \
return __TYPE__::bm; \
}

*/


/**

#define GET_BULLET_FUNC(__BULLET_TYPE__) \
__BULLET_TYPE__* get_bullet() \
{ \
virtual __BULLET_TYPE__* ret = nullptr; \
if (v_blt_wsy.empty()) { \
ret = __BULLET_TYPE__::create(); \
} \
else { \
ret = v_blt_wsy.back(); \
v_blt_wsy.pop_back(); \
} \
v_blt_zsy.push_back(ret); \
return ret; \
}

*/


//################################################################################//


//敌机子弹管理类
class C_FoeBUlletManager :public C_BulletManager
{
public:
	static C_FoeBUlletManager* getInstance();
	//GETINSTANCE_FUNC(C_FoeBUlletManager);
	virtual C_Bullet* get_bullet();
	//GET_BULLET_FUNC(C_HuangDan)
	virtual void recovery(C_Bullet*);

	virtual int crash_recovery(Point , float);	//碰撞检测并回收
private:
	C_FoeBUlletManager() {};
	~C_FoeBUlletManager() {};
private:
	static C_FoeBUlletManager * bm;
};

//################################################################################//

//玩家子弹管理类
class C_PlayerBUlletManager :public C_BulletManager
{
public:
	static C_PlayerBUlletManager* getInstance();
	//GETINSTANCE_FUNC(C_PlayerBUlletManager);
	virtual C_Bullet* get_bullet();
	//GET_BULLET_FUNC(C_LanDan)
	virtual C_Bullet* get_DaoDan();
	virtual void recovery(C_Bullet*);

	virtual int crash_recovery(Point , float);	//碰撞检测并回收
private:
	C_PlayerBUlletManager() {};
	~C_PlayerBUlletManager() {};
protected:
	std::vector<C_Bullet*> v_dd_wsy;	//未使用的导弹
	std::list<C_Bullet*> v_dd_zsy;	//正使用的导弹
private:
	static C_PlayerBUlletManager * bm;
};



#endif //__C_BULLETMANAGER_H_
