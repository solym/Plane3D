/****************************************************************************
传智博客C++学院
****************************************************************************/

#ifndef __Moon3d__Sprite3DEffect__
#define __Moon3d__Sprite3DEffect__

#include <iostream>

#include "cocos2d.h"

USING_NS_CC;

class EffectSprite3D;

class Effect3D : public Ref//3D效果类，纯虚函数，类似接口
{
public:
	virtual void drawWithSprite(EffectSprite3D* sprite, const Mat4 &transform) = 0;//渲染带特效的精灵
protected:
	Effect3D() : _glProgramState(nullptr) {}//构造函数
	virtual ~Effect3D()//析构函数
	{
		CC_SAFE_RELEASE(_glProgramState);//清除变量内存占用
	}
protected:
	GLProgramState* _glProgramState;//定义opengl变量
};

class Effect3DOutline : public Effect3D//3d效果边线类
{
public:
	static Effect3DOutline* create();//创建3d特效边线

	void setOutlineColor(const Vec3& color);//设置3d特效边线颜色

	void setOutlineWidth(float width);//设置3d特效边线宽度

	void drawWithSprite(EffectSprite3D* sprite, const Mat4 &transform);//渲染带特效的精灵

protected:

	Effect3DOutline();//构造函数
	virtual ~Effect3DOutline();//析构函数

	bool init();//初始化

	Vec3 _outlineColor;//定义3d边线颜色
	float _outlineWidth;//定义3d边线宽度
public:
	static const std::string _vertShaderFile;//定义顶点着色器文件
	static const std::string _fragShaderFile;//定义片断着色器文件
	static const std::string _keyInGLProgramCache;//定义opengl程序缓存
	static GLProgram* getOrCreateProgram();//获取opengl程序实例

};

class EffectSprite3D : public Sprite3D//带特效的3d精灵
{
public:
	static EffectSprite3D* createFromObjFileAndTexture(const std::string& objFilePath, const std::string& textureFilePath);//创建特效精灵
	//	static EffectSprite3D* createFromC3bFileAndTexture(const std::string& objFilePath, const std::string& textureFilePath);
	void setEffect3D(Effect3D* effect);//设置特效精灵的3d特效
	void addEffect(Effect3D* effect, ssize_t order);//把3d特效添加到特效精灵的集合里
	void eraseEffect(Effect3D* effect);//删除特效精灵的某个特效
	ssize_t getEffectCount() const;//获取特效的数量
	Effect3D* getEffect(ssize_t index) const;//获取某个特定的特效
	virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;//渲染
protected:
	EffectSprite3D();//构造函数
	virtual ~EffectSprite3D();//析构函数

	std::vector<std::tuple<ssize_t, Effect3D*, CustomCommand>> _effects;//定义储存3d特效的集合
	Effect3D* _defaultEffect;//定义3d特效
	CustomCommand _command;//定义渲染变量集合
};

#endif /* defined(__Moon3d__Sprite3DEffect__) */
