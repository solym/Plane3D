//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_Bullet.h"
#include "C_Particle.h"
#include "C_BulletManager.h"
#include "C_FoeManager.h"
#if 1

//子弹爆炸效果
void C_Bullet::boom(float strength)
{
	//爆炸烟雾效果
	ParticleSystemQuad * b = ParticleSystemQuad::create("toonSmoke.plist");
	this->getParent()->addChild(b);
	b->setPosition(getPosition());
	b->setScale(strength);
	//爆炸碎片粒子效果
	//ParticleSystem * d = C_Particle::getInstance()->add_particle("debris.plist");
	//this->getParent()->addChild(d);
	//d->setPosition(getPosition());
}

//################################################################################//
//子弹朝向一个角度飞出
void C_Bullet::fly_to(float angle)
{

	//MoveBy::create()
}

//子弹朝向一个目标位置飞出
void C_Bullet::fly_to(Point to)
{
	Vec2 d = to - getPosition();
	//调整子弹朝向飞行方向
	float angle = d.y < 0 ? 180.0f : 0.0f;
	angle += d.getAngle();
	setRotation(angle);
	//往目的地飞，其速度;
	v_speed = d.length() / 300.0f;
	CallFunc * f = CallFunc::create(
		[this]()
	{
		this->boom();
		if (getParent()) {
			this->removeFromParentAndCleanup(false);	//摘除但不释放，可以提高效率
		}
		C_BulletManager::recovery(this);	//子弹管理器回收子弹

	}
	);
	runAction(Sequence::createWithTwoActions(MoveBy::create(v_speed , d) , f));
}



//################################################################################//
//黄色子弹的初始化
bool C_HuangDan::init()
{
	//设置纹理
	Texture2D * t = Director::getInstance()->getTextureCache()->addImage("bullets.png");
	//调用基类构造
	if (!Sprite::initWithTexture(t , Rect(12.0f , 12.0f , 16.0f , 30.0f))) {
		return false;
	}
	//设置可回收状态和子弹类型
	//v_khs = false;
	//v_type = BulletType::Huang;

	//设置尾部发光粒子
	ParticleSystem * ps = C_Particle::getInstance()->add_particle("glow.plist");
	addChild(ps);
	ps->setPosition(Vec2(6.0f , 8.0f));//设置粒子位置
	ps->setScale(0.2f);	//设置缩放
	ps->setTotalParticles(5);	//设置粒子总数
	ps->setEmissionRate(5.0f);	//设置粒子发射速度
	ps->setDuration(10000.0f);	//设置粒子效果持续时间
	ps->setPositionType(ParticleSystem::PositionType::GROUPED);	//设置位置类型
	//setScale(0.5f);
	return true;

}


//################################################################################//

//蓝色子弹的初始化
bool C_LanDan::init()
{
	//设置纹理
	Texture2D * t = Director::getInstance()->getTextureCache()->addImage("bullets.png");
	if (!Sprite::initWithTexture(t , Rect(64.0f , 60.0f , 20.0f , 45.0f))) {
		return false;
	}
	//设置可回收状态和子弹类型
	//v_khs = false;
	//v_type = BulletType::Lan;

	setScale(0.7f);
	return true;
	////设置尾部发光粒子
	//ParticleSystem * ps = C_Particle::getInstance()->add_particle("flare.plist");
	//addChild(ps);
	//ps->setPosition(Vec2(8.0f , .0f));
}

//################################################################################//


//################################################################################//
//导弹的初始化
bool C_DaoDan::init()
{
	if (!Sprite::init()) {
		return false;
	}
	//设置可回收状态和子弹类型
	//v_khs = false;
	//v_type = BulletType::Dao;

	v_daodan = Sprite3D::create("daodanv001.obj" , "daodan_32.png");
	addChild(v_daodan);
	v_daodan->setAnchorPoint(Vec2(0.0f , .0f));
	v_daodan->setPosition(Vec2::ZERO);
	setScale(3.0f);
	v_daodan->setRotation3D(Vec3(35.0f,0.0f,-5.0f));
	//设置尾部发光粒子
	ParticleSystem * ps = C_Particle::getInstance()->add_particle("glow.plist");
	v_daodan->addChild(ps);
	ps->setPosition(Vec2(0.0f , -5.0f));//设置粒子位置
	ps->setScale(0.1f);	//设置缩放
	ps->setTotalParticles(5);
	ps->setEmissionRate(5.0f);
	ps->setDuration(10000.0f);	//设置粒子效果持续时间
	ps->setPositionType(ParticleSystem::PositionType::GROUPED);	//设置位置类型
	ps->setRotation3D(Vec3(125.0f , 0.0f , -5.0f));	//设置粒子角度
	return true;
}


//导弹的飞向一个点
void C_DaoDan::fly_to(Point to)
{
	bool left = rand() % 2==1;
	Vec2 ddt = left ? Vec2(-50.0f , -30.0f) : Vec2(50.0f , -30.0f);

	MoveBy * dt = MoveBy::create(0.5f , ddt);
	CallFunc * f = CallFunc::create([this,to](){this->C_Bullet::fly_to(to); });
	runAction(Sequence::createWithTwoActions(dt , f));
}

//################################################################################//
//导弹追踪一个目标
void C_DaoDan::fly_to(Node* target,float dist)
{
	if (target == nullptr) {
		Point to = Director::getInstance()->getWinSize();
		to.x /= 2;
		to.y *= 2;
		C_DaoDan::fly_to(to);
		return;
	}
	bool left = rand() % 2==1;
	Vec2 ddt = left ? Vec2(-50.0f , -30.0f) : Vec2(50.0f , -30.0f);

	MoveBy * dt = MoveBy::create(0.5f , ddt);

	v_target = target;
	v_dist = dist;

	CallFunc * f = CallFunc::create(
		[this , target , dist](){
		schedule(schedule_selector( C_DaoDan::zido) , 0.05f , 100 , 0.0f);//开启定时器
	}
	);
	runAction(Sequence::createWithTwoActions(dt , f));
}

//################################################################################//
//自动追踪定时器回调
void C_DaoDan::zido(float dt)
{
	Vec2 tp = v_target->getPosition();
	Size winsize = Director::getInstance()->getWinSize();
	
	//如果目标已经到了屏幕外面，更改目标
	if (tp.x > winsize.width || tp.x<0 ||
		tp.y>winsize.height || tp.y < 0) {
		unschedule(schedule_selector(C_DaoDan::zido));
		fly_to(Point(getPosition().x , winsize.height));
		/**	太占资源了，目标走了就直接fly_to
		v_target = C_FoeManager::getInstance()->get_one_run_foe();
		*/
	}
	

	Vec2 diff = v_target->getPosition() - this->getPosition();
	
	if (diff.getLengthSq() < v_dist*v_dist) {
		this->boom();	//爆炸
		if (getParent()) {	//无需判断，removeFromParentAndCleanup内部会做判断
			this->removeFromParentAndCleanup(false);
		}
		C_BulletManager::recovery(this);	//回收
		unschedule(schedule_selector(C_DaoDan::zido));
		return;
	}
	Vec2 yidong(15.0f*cos(diff.getAngle()) , 15.0f*sin(diff.getAngle()));
	this->setPosition(this->getPosition() + yidong);
	this->setRotation(diff.getAngle());
	return;
}

#endif