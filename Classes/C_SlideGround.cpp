//	2014年11月29日 14:07:19
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_SlideGround.h"

bool C_SlideGround::init()
{
	//初始化背景图片精灵
	if (!Sprite::initWithTexture(Director::getInstance()->getTextureCache()->addImage("groundLevel.png")))
	{
		return false;
	}
	
	//宽度适应
	setScale(Director::getInstance()->getWinSize().width / getContentSize().width);
	//设置默认锚点
	setAnchorPoint(Vec2::ZERO);
	return true;
}

void C_SlideGround::slide_bg()
{
	//位置回退
	std::function<void(void)> ht = [this](){
		this->setPosition(Vec2::ZERO);
	};
	MoveBy *mb = MoveBy::create(7.0f , Vec2(0.0f , -getBoundingBox().size.height / 2));
	runAction(RepeatForever::create(Sequence::createWithTwoActions(CallFunc::create(ht) , mb)));
}

void C_SlideGround::stop_slide()
{
	this->stopAllActions();
}